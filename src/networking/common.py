import asyncio
import jsonpickle

from collections import defaultdict
from typing import Dict, DefaultDict, Callable, Any, Optional, List


class IO(asyncio.Protocol):
    client_map: Dict[int, "IO"] = {}
    callback_map: DefaultDict[str, List[Callable[[str, "IO"], None]]] = defaultdict(
        list
    )

    def __init__(self, on_connection_lost=None):
        self.on_connection_lost = on_connection_lost

    def __call__(self):
        return self

    def __str__(self):
        return f"{self.ip}:{self.socket_id}"

    def connection_made(self, transport):
        self.transport = transport
        self.peername = transport.get_extra_info("peername")
        self.ip = self.peername[0]
        self.socket_id = self.peername[1]
        self.client_map[self.socket_id] = self

    def data_received(self, data):
        message = jsonpickle.decode(data)
        [fn(message, self) for fn in IO.callback_map[message.get("id")]]

    def connection_lost(self, exc):
        IO.client_map.pop(self.socket_id)

        if self.on_connection_lost is not None:
            self.on_connection_lost.set_result(True)

    @staticmethod
    def on(id_message, fn):
        IO.callback_map[id_message].append(fn)

    @staticmethod
    def broadcast(message):
        for _, client in IO.client_map.items():
            client.write(message)

    @staticmethod
    def write_to(message: Any, client_id: int):
        IO.client_map[client_id].write(message)

    @staticmethod
    def write_to_clients(message, clients: List[int]):
        for client in clients:
            IO.write_to(message, client)

    def broadcast_to_others(self, message):
        for _, client in self.client_map.items():
            if client == self:
                continue
            client.write(message)

    def write(self, message: Any):
        self.transport.write(jsonpickle.encode(message).encode())
