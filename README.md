# AI12 - Nomad

## Makefile

The Makefile will automatically handle the virtual env. Type `make` to see the available commands. The Makefile only supports unix-based operating systems. If you're on Windows, consider using WSL2.

## Set up the CI

Because of Gitlab's limitations, we have to ensure that each fork is within the same `group`. Projects within the group will be able to access the CI dedicated to this project which is a requirement to make a Merge Request (MR).

1. Request to join the AI12-nomad Group https://gitlab.com/AI12-Nomad
2. Change the name of the fork and its URL to something unique (Settings -> Project Name, Settings -> Advanced -> Change Path)
3. Transfer the ownership to the AI12-nomad group. (Settings -> Advanced -> Transfert ownership)
4. Update your remotes with the new URL if necessary.

## Setup git

1. [Add an ssh key to your gitlab account](https://docs.gitlab.com/ee/ssh/)
2. `git clone git@gitlab.com:AI12-Nomad/ai12-nomad.git`
3. [Fork the repository](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)
4.  Don't forget to change the {user_name} **without the curly brackets**!

Add the forked repository as a remote `git remote add fork  git@gitlab.com:{user_name}/ai12-nomad.git`

5. Consider disabling push on `origin`: `git remote set-url --push origin no_push`

## Contribute

1. To make changes start with creating a branch `git checkout -b my_feature origin/dev`.
2. Push to your fork using the same branch name. `git push fork my_feature`
3. [Create a Merge Request on gitlab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-use-git-commands-locally)
